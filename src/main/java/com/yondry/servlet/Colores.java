package com.yondry.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.Request;

public class Colores extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4549783843509154803L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	PrintWriter out = resp.getWriter();
	String pnum = req.getParameter("pnum");
	String snum = req.getParameter("snum");
	
	int num1 = Integer.parseInt(pnum);
	int num2 = Integer.parseInt(snum);
	
	
	int numero = num1+num2;
	
	if(numero%2 == 0){
	req.getRequestDispatcher("/colores/azul.jsp").forward(req, resp);	
	}else{
	req.getRequestDispatcher("/colores/verde.jsp").forward(req, resp);
	}
	
	req.setAttribute("pnum", pnum);
	req.setAttribute("snum", snum);
	}
	
	
	
}
